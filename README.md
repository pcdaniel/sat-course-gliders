# CoastWatch Satellite Course 2022 Project #

### Analysis of glider surface temperature compared with several SST Products. ###

__Patrick Daniel__
----
Code can be found in [explore-glider.ipynb](./explore-glider.ipynb).

Glider Data:  
- [IOOS Glider Map ERDDAP](https://gliders.ioos.us/erddap/tabledap/sp025-20211208T2324.html)  

Satellite Data - Coastwatch ERDDAP:
- [NASA MUR](https://coastwatch.pfeg.noaa.gov/erddap/griddap/jplMURSST41.graph)
- [AQUA MODIS, daytime](https://coastwatch.pfeg.noaa.gov/erddap/griddap/erdMBsstd1day.graph)
- [NOAA GeoPolar](https://coastwatch.pfeg.noaa.gov/erddap/info/nesdisGeoPolarSSTN5SQNRT/index.html)


See [Project Slide](./pdaniel-remote-sensing.pdf)
![](./figures/temp_diff_map_geo.png)
![](./figures/temp_scatter.png)
